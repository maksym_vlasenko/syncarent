﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace RentApp.Data.Entities
{
    public class RentUser : IdentityUser<int>
    {
        public double Raiting { get; set; }

        public IEnumerable<RentInformation> Rents { get; set; }

        public IEnumerable<Announcement> Announcements { get; set; }

        public UserInformation UserInformation { get; set; }

        public int UserInformationId { get; set; }

        public List<CompabilityRequest> CompabilityRequests { get; set; }
    }
}
