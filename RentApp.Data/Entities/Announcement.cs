﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentApp.Data.Entities
{
    public class Announcement
    {
        public int AnnouncementId { get; set; }

        public DateTime CreatedAt { get; set; }

        public int Price { get; set; }

        public string Title{ get; set; }

        public string Description { get; set; }

        public string City { get; set; }

        public string Status { get; set; }

        public int RealtyInformationId { get; set; }

        public int RentUserId { get; set; }

        public RealtyInformation RealtyInformation { get; set; }

        public RentUser RentUser { get; set; }

        public List<RentInformation> Rents { get; set; }

        public List<CompabilityRequest> CompabilityRequests { get; set; }
    }
}
