﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentApp.Data.Entities
{
    public class ImageInfo
    {
        public int ImageInfoId { get; set; }

        public string Path { get; set; }

        public string Name { get; set; }

        public int RealtyInformationId { get; set; }

        public RealtyInformation RealtyInformation { get; set; }
    }
}
