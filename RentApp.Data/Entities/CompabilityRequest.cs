﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentApp.Data.Entities
{
    public class CompabilityRequest
    {
        public int CompabilityRequestId { get; set; }

        public Announcement Announcement { get; set; }

        public int AnnouncementId { get; set; }

        public RentUser User { get; set; }

        public int UserId { get; set; }
    }
}
