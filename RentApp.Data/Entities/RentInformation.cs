﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RentApp.Data.Entities
{
    public class RentInformation
    {
        public int RentInformationId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int AnnouncementId { get; set; }

        public int RentUserId { get; set; }

        public Announcement Announcement { get; set; }

        [ForeignKey("RentUserId")]
        public RentUser RentUser { get; set; }
    }
}
