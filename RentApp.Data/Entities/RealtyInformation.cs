﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentApp.Data.Entities
{
    public class RealtyInformation
    {
        public int RealtyInformationId { get; set; }

        public int RoomCount { get; set; }

        public int Year { get; set; }

        public double Square { get; set; }

        public string Address { get; set; }

        public string HeatingType { get; set; }

        public List<ImageInfo> Images { get; set; }
    }
}
