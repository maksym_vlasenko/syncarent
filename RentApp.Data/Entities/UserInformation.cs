﻿namespace RentApp.Data.Entities
{
    public class UserInformation
    {
        public int UserInformationId { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public int Age { get; set; }

        public string Sex { get; set; }

        public string Temperament { get; set; }

        public bool Cook { get; set; }

        public string Activity { get; set; }

        public string Rest { get; set; }

        public string Status { get; set; }

        public int Konflict { get; set; }

        public bool Pet { get; set; }

        public string Review { get; set; }
    }
}
