﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RentApp.Data.Entities;

namespace RentApp.Data.EntityFramework
{
    public class RentContext : IdentityDbContext<RentUser, RentUserRole, int>
    {
        public DbSet<Announcement> Announcements { get; set; }

        public DbSet<RealtyInformation> RealtyInformations { get; set; }


        public DbSet<RentInformation> RentInformations { get; set; }

        public DbSet<ImageInfo> Images { get; set; }

        public DbSet<UserInformation> UserInformation { get; set; }

        public DbSet<CompabilityRequest> CompabilityRequest { get; set; }

        public RentContext(DbContextOptions<RentContext> options)
            : base(options)
        {

        }

       /* protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RentUser>()
                .HasOne(a => a.Biography)
                .WithOne(b => b.Author)
                .HasForeignKey<AuthorBiography>(b => b.AuthorRef);
        }*/
    }
}
