﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using RentApp.Data.Entities;
using RentApp.Data.EntityFramework;
using RentApp.Models;

namespace RentApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly RentContext _context;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<RentUser> _signInManager;
        private readonly UserManager<RentUser> _userManager;
        private IStorage _storage;

        public AccountController(
            RentContext context,
            IConfiguration configuration,
            UserManager<RentUser> userManager,
            SignInManager<RentUser> signInManager,
            IStorage storage)
        {
            _context = context;
            _configuration = configuration;
            _signInManager = signInManager;
            _userManager = userManager;
            _storage = storage;
        }

        /// <summary>
        /// Register new User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     api\Registration
        ///       {
        ///        "login" : "testlogin",
        ///        "password": "Testpass"
        ///        }
        /// </remarks>
        /// <response code="201">User registered</response> 
        /// <response code="400">Bad request</response> 
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("Registration")]
        public async Task<IActionResult> Registration(ApplicationUserModel model)
        {
            if (await _userManager.FindByNameAsync(model.Login) == null)
            {
                var isCook = model.Profile.Cook.Equals("Так");
                var isPet = model.Profile.Pet.Equals("Так");
                var newUser = new RentUser() 
                { 
                    UserName = model.Login,
                    PhoneNumber = model.Profile.Phone,
                    UserInformation = new UserInformation()
                    {
                        Status = model.Profile.Status,
                        Age = int.Parse(model.Profile.Age),
                        Temperament = model.Profile.Temperament,
                        Cook = isCook,
                        Activity = model.Profile.Activity,
                        Rest = model.Profile.Rest,
                        Konflict = int.Parse(model.Profile.Conflict),
                        Pet = isPet,
                        Review = model.Profile.Review,
                        FirstName = model.Profile.Name,
                        Lastname = model.Profile.Surname,
                        Sex = model.Profile.Sex
                    }
                };

                await _userManager.CreateAsync(newUser, model.Password);

                return Ok();
            }

            return BadRequest("Користувач з таким логіном вже існує в системі.");
        }

        /// <summary>
        /// Login User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     api\Login
        ///       {
        ///        "login" : "testlogin",
        ///        "password": "Testpass"
        ///        }
        /// </remarks>
        /// <response code="200">JWT Token</response> 
        /// <response code="400">Bad request</response> 
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("Login")]
        public async Task<IActionResult> Login(ApplicationUserModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, false, false);
            if (result.Succeeded)
            {
                var loginedUser= new RentUser{UserName = model.Login};

                var token = await GetJWT(loginedUser);

                return Ok(token);
            }

            return BadRequest("Wrong login or password");
          // return Ok(_storage.Login());
        }

        [HttpPost("CreateProfile")]
        public async Task<IActionResult> CreateProfile(CompatibleUserProfile profile)
        {

            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            var k = user.UserInformation;
            return Ok();
            // return Ok(_storage.Login());
        }



        private async Task<object> GetJWT(RentUser user)
        {
            var tokenTimeCreated = DateTime.UtcNow;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
            };

            string secretKey = _configuration["Jwt:Key"];
            int expires = int.Parse(_configuration["Jwt:Expires"]);

            var jwt = new JwtSecurityToken(
                issuer: "Me",
                audience: "My Api",
                notBefore: tokenTimeCreated,
                claims: claims,
                expires: tokenTimeCreated.AddMinutes(expires),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(secretKey)),
                    SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
    }
}
