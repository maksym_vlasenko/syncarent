﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RentApp.Data.Entities;
using RentApp.Data.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentApp.Models;

namespace RentApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompatibleController : ControllerBase
    {
        private readonly RentContext _context;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<RentUser> _signInManager;
        private readonly UserManager<RentUser> _userManager;
        private readonly IStorage _storage;

        public CompatibleController(
            RentContext context,
            IConfiguration configuration,
            UserManager<RentUser> userManager,
            SignInManager<RentUser> signInManager,
            IStorage storage)
        {
            _context = context;
            _configuration = configuration;
            _signInManager = signInManager;
            _userManager = userManager;
            _storage = storage;
        }

        [HttpGet("{announcementId}")]
        public async Task<IActionResult> Get(int announcementId)
        {
            var currentUserr = await _userManager.FindByNameAsync(User.Identity.Name);
            var currentUser =
                await _context.UserInformation.FirstOrDefaultAsync(x =>
                    x.UserInformationId == currentUserr.UserInformationId);

            var users = await _context.CompabilityRequest
                .Include(x => x.User)
                .ThenInclude(x => x.UserInformation)
                .Where(r => r.AnnouncementId == announcementId)
                .Select(x => x.User)
                .ToListAsync();

            var compabilityScore = await GetCompability(currentUser, users.Select(x => x.UserInformation));

            var controllerResult = compabilityScore.Select((x,index) => new CompabilityControllerResult()
            {
                Name = x.Key.FirstName,
                Surname = x.Key.Lastname,
                Sex = x.Key.Sex,
                Age = x.Key.Age.ToString(),
                Review = x.Key.Review,
                Compability = x.Value,
                Phone = users[index].PhoneNumber
            });

            return Ok(controllerResult);
        }

        [HttpPost("{announcementId}")]
        public async Task<IActionResult> Post(int announcementId)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);


            var compRequest = new CompabilityRequest()
            {
                UserId = user.Id,
                AnnouncementId = announcementId
            };

            _context.CompabilityRequest.Add(compRequest);
            await _context.SaveChangesAsync();

            return StatusCode(201);
        }

        private async Task<Dictionary<UserInformation, int>> GetCompability(UserInformation currentUser, IEnumerable<UserInformation> users)
        {
            var result = new Dictionary<UserInformation, int>();
            foreach (var user in users)
            {
                var sum = 0;

                sum += GetTemperamentScore(currentUser.Temperament, user.Temperament);

                if (user.Pet == currentUser.Pet)
                {
                    sum+=10;
                }

                if (Math.Abs(user.Age - currentUser.Age) < 7)
                {
                    sum += 10;
                }

                if (currentUser.Cook && user.Cook)
                {
                    sum += 10;
                }

                if (string.Equals(currentUser.Activity, user.Activity))
                {
                    sum += 15;
                }

                if (string.Equals(currentUser.Status, user.Status) && string.Equals(currentUser.Status, "Самотній"))
                {
                    sum += 13;
                }

                sum += 12 - (currentUser.Konflict + user.Konflict);

                result.Add(user, sum);
            }

            return result;
        }

        private int GetTemperamentScore(string current, string other)
        {
            if (string.Equals(current, "Холерик"))
            {
                switch (other)
                {
                    case "Холерик":
                        return TemperamentScore.Bad;
                    case "Сангвінік":
                        return TemperamentScore.Good;
                    case "Флегматик":
                        return TemperamentScore.VeryGood;
                    case "Меланхолік":
                        return TemperamentScore.Bad;
                }
            }
            else if (string.Equals(current, "Сангвінік"))
            {
                switch (other)
                {
                    case "Холерик":
                        return TemperamentScore.Good;
                    case "Сангвінік":
                        return TemperamentScore.Good;
                    case "Флегматик":
                        return TemperamentScore.Bad;
                    case "Меланхолік":
                        return TemperamentScore.VeryGood;
                }
            }
            else if (string.Equals(current, "Флегматик"))
            {
                switch (other)
                {
                    case "Холерик":
                        return TemperamentScore.VeryGood;
                    case "Сангвінік":
                        return TemperamentScore.Bad;
                    case "Флегматик":
                        return TemperamentScore.VeryGood;
                    case "Меланхолік":
                        return TemperamentScore.Good;
                }
            }
            else if (string.Equals(current, "Меланхолік"))
            {
                switch (other)
                {
                    case "Холерик":
                        return TemperamentScore.Bad;
                    case "Сангвінік":
                        return TemperamentScore.VeryGood;
                    case "Флегматик":
                        return TemperamentScore.Good;
                    case "Меланхолік":
                        return TemperamentScore.VeryGood;
                }
            }

            return 0;
        }

    }
    public static class TemperamentScore
    {
        public const int VeryGood = 30;
        public const int Good = 15;
        public const int Bad = 0;
    }
}

