﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RentApp.Data.Entities;
using RentApp.Data.EntityFramework;
using RentApp.Models;

namespace RentApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RentController : ControllerBase
    {
        private readonly RentContext _context;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<RentUser> _signInManager;
        private readonly UserManager<RentUser> _userManager;
        private readonly IStorage _storage;

        public RentController(
            RentContext context,
            IConfiguration configuration,
            UserManager<RentUser> userManager,
            SignInManager<RentUser> signInManager,
            IStorage storage)
        {
            _context = context;
            _configuration = configuration;
            _signInManager = signInManager;
            _userManager = userManager;
            _storage = storage;
        }

        [HttpPost]
        public async Task<IActionResult> Rent([FromBody]RentRequest rent)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var announcement = await _context.Announcements
                .FirstOrDefaultAsync(x => x.AnnouncementId == rent.AnnouncementId);
             _context.RentInformations.Add(new RentInformation()
            {
                RentUserId = user.Id,
                FromDate = rent.FromDate.Date,
                ToDate = rent.ToDate.Date,
                AnnouncementId = rent.AnnouncementId

            });
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
             var user = await _userManager.FindByNameAsync(User.Identity.Name);

            var announcements = await _context.RentInformations.Include(x=>x.Announcement)
                .ThenInclude(x => x.RealtyInformation)
                .ThenInclude(x => x.Images)
                .Where(rent => rent.RentUserId == user.Id)
                .ToListAsync();

            return Ok(announcements);
        }
    }
}
