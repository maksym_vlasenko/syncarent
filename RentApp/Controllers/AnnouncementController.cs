﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Query.Internal;
using Microsoft.Extensions.Configuration;
using RentApp.Data.Entities;
using RentApp.Data.EntityFramework;
using RentApp.Models;

namespace RentApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AnnouncementController : ControllerBase
    {
        private readonly RentContext _context;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<RentUser> _signInManager;
        private readonly UserManager<RentUser> _userManager;
        private readonly IStorage _storage;

        public AnnouncementController(
            RentContext context,
            IConfiguration configuration,
            UserManager<RentUser> userManager,
            SignInManager<RentUser> signInManager,
            IStorage storage)
        {
            _context = context;
            _configuration = configuration;
            _signInManager = signInManager;
            _userManager = userManager;
            _storage = storage;
        }

        /// <summary>
        /// GetList Announcment.
        /// </summary>
        /// <response code="200">List</response> 
        /// <response code="400">Bad request</response>
        [HttpPost("")]
        public async Task<IActionResult> GetAnnouncments([FromBody] SearchFilter filter)
        {
            SetupFilterDefaults(filter);
            var announcement = _context.Announcements.Include(x => x.RealtyInformation).ThenInclude(x => x.Images)
                .Where(x => x.Price >= filter.FromPrice && x.Price <= filter.ToPrice &&
                            x.RealtyInformation.RoomCount >= filter.FromRoomCount &&
                            x.RealtyInformation.RoomCount <= filter.ToRoomCount &&
                            x.RealtyInformation.Year >= filter.FromYear &&
                            x.RealtyInformation.Year <= filter.ToYear &&
                            x.RealtyInformation.Square >= filter.FromSquare &&
                            x.RealtyInformation.Square <= filter.ToSquare);

            if (!filter.City.Equals(string.Empty) /*|| !filter.City.Equals("string")*/)
            {
                announcement = announcement.Where(x => string.Equals(x.City, filter.City));
            }
            if (!filter.HeatingType.Equals(string.Empty) /*|| !filter.HeatingType.Equals("string")*/)
            {
                announcement = announcement.Where(x => string.Equals(x.RealtyInformation.HeatingType, filter.HeatingType));
            }

            announcement = announcement.Where(x => !x.Rents.Any(r =>
                (r.FromDate.Date > filter.FromDate.Date && r.FromDate.Date < filter.ToDate.Date) ||
                (r.ToDate.Date > filter.FromDate.Date && r.ToDate.Date < filter.ToDate.Date) ||
                (r.FromDate.Date < filter.FromDate.Date && r.ToDate.Date > filter.ToDate.Date)));

            var filteredAnnouncementCount = announcement.Count();

            var pagingResult = announcement
                .Skip((filter.Page - 1) * filter.Items)
                .Take(filter.Items).ToList();

            var pagResult = new
            {
                Items = pagingResult,
                TotalItems = filteredAnnouncementCount
            };

            return Ok(pagResult);
        }

        /// <summary>
        /// Get Announcment by Id.
        /// </summary>
        /// <response code="200">List</response> 
        /// <response code="400">Bad request</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAnnouncments(int id)
        {
             var announcement = await _context.Announcements
                 .Include(y => y.RealtyInformation)
                 .ThenInclude(t => t.Images)
                 .FirstOrDefaultAsync(x => x.AnnouncementId == id);
            //var an = _storage.GetById(id);

            return Ok(announcement);
        }


        /// <summary>
        /// Create new announcment.
        /// </summary>
        /// <response code="200">List</response> 
        /// <response code="400">Bad request</response>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromForm] AnnAndFile model)
        {

            string startupPath = Path.Combine(Directory.GetCurrentDirectory(), "Files");

             var user = await _userManager.FindByNameAsync(User.Identity.Name);

            Account account = new Account(
           "didqycjsn",
           "564915911836332",
           "daBRLvIN9fJAWgpCD_Nm8JtAe_g");

            Cloudinary cloudinary = new Cloudinary(account);
            var filesToUpload = new List<Data.Entities.ImageInfo>();

            if (model.Files != null)
            {
                foreach (var file in model.Files)
                {

                    using (var stream = file.OpenReadStream())
                    {
                        var uploadParams = new ImageUploadParams()
                        {
                            File = new FileDescription(file.FileName, stream)
                        };
                        var uploadResult = cloudinary.Upload(uploadParams);

                        var newFile = new Data.Entities.ImageInfo
                        {
                            Name = file.FileName,
                            Path = uploadResult.Uri.ToString()
                        };

                        filesToUpload.Add(newFile);
                    }
                }
            }
            
            // _storage.Add(model.Announcement);
            var annoumcement = new Announcement()
            {
                CreatedAt = DateTime.Now,
                RentUserId = user.Id,
                City = model.Announcement.City,
                Description = model.Announcement.Description,
                Price = model.Announcement.Price,
                Status = model.Announcement.Status,
                Title = model.Announcement.Title,
                RealtyInformation = new RealtyInformation()
                {
                    Address = model.Announcement.RealtyInformation.Address,
                    HeatingType = model.Announcement.RealtyInformation.HeatingType,
                    RoomCount = model.Announcement.RealtyInformation.RoomCount,
                    Square = model.Announcement.RealtyInformation.Square,
                    Year = model.Announcement.RealtyInformation.Year,
                    Images = filesToUpload
                }
            };

            await _context.Announcements.AddAsync(annoumcement);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private void SetupFilterDefaults(SearchFilter filter)
        {
            if (filter.ToRoomCount == 0)
                filter.ToRoomCount = Int32.MaxValue;

            if (filter.ToSquare == 0)
                filter.ToSquare = Int32.MaxValue;

            if (filter.ToYear == 0)
                filter.ToYear = Int32.MaxValue;

            if (filter.ToPrice == 0)
                filter.ToPrice = Int32.MaxValue;

            if (filter.Page == 0)
                filter.Page = 1;

            if (filter.Items == 0)
                filter.Items = 10;
        }

        private void CorrectModel(Announcement an)
        {
            an.Rents = null;
        }
    }
}
