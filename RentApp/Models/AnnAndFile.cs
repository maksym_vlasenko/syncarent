﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using RentApp.Data.Entities;

namespace RentApp.Models
{
    public class AnnAndFile
    {
        public AnnouncementViewModel Announcement { get; set; }

        public IFormFileCollection Files { get; set; }
    }
}
