﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentApp.Models
{
    public class ApplicationUserModel
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public CompatibleUserProfile Profile { get; set; }
    }
}
