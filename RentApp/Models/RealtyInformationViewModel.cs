﻿
namespace RentApp.Models
{
    public class RealtyInformationViewModel
    {
        public int RoomCount { get; set; }

        public int Year { get; set; }

        public double Square { get; set; }

        public string Address { get; set; }

        public string HeatingType { get; set; }
    }
}
