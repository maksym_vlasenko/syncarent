﻿
using System;

namespace RentApp.Models
{
    public class SearchFilter
    {
        public int FromPrice { get; set; }

        public int ToPrice { get; set; } = Int32.MaxValue;

        public string City { get; set; }

        public string HeatingType { get; set; }

        public int FromSquare { get; set; }

        public int ToSquare { get; set; } = Int32.MaxValue;

        public int FromYear { get; set; }

        public int ToYear { get; set; } = Int32.MaxValue;

        public int FromRoomCount { get; set; }

        public int ToRoomCount { get; set; } = Int32.MaxValue;

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int Page { get; set; } = 1;

        public int Items { get; set; } = 10;
    }
}
