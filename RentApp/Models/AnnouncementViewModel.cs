﻿using System;

namespace RentApp.Models
{
    public class AnnouncementViewModel
    {
        public int AnnouncementId { get; set; }

        public int Price { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string City { get; set; }

        public string Status { get; set; }

        public RealtyInformationViewModel RealtyInformation { get; set; }
    }
}
