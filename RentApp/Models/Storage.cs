﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using RentApp.Data.Entities;

namespace RentApp.Models
{
    public class Storage : IStorage
    {
        private static int anCounter = 3;
        private static int rentCounter = 1;
        private static int imageCounter = 6;
        public List<Announcement> storage = new List<Announcement>()
        {
            new Announcement()
            {
                AnnouncementId = 1,
                CreatedAt = DateTime.Now.AddDays(-1),
                Price = 100,
                RealtyInformationId = 1,
                RentUserId = 1,
                City = "Вінниця",
                Title = "Оренда квартири дешево писати лс",
                Description = "Тут тіпа тексту речень 6 буде",
                RealtyInformation = new RealtyInformation()
                {
                    RealtyInformationId = 1,
                    HeatingType = "Центральне",
                    Square = 40,
                    Year = 2009,
                    RoomCount = 2,
                    Address = "Соборна 55/12",
                    Images = new List<ImageInfo>()
                    {
                        new ImageInfo()
                        {
                            ImageInfoId = 1,
                            Name = "Test1.png",
                            Path = "http://res.cloudinary.com/didqycjsn/image/upload/v1617222992/uypilxn7u2dcb469qyi2.png",
                            RealtyInformationId = 1
                        },
                        new ImageInfo()
                        {
                            ImageInfoId = 2,
                            Name = "Test2.png",
                            Path = "http://res.cloudinary.com/didqycjsn/image/upload/v1617223053/mm0ws5jkrlixraialg1g.png",
                            RealtyInformationId = 1
                        }
                    }
                }

            },
            new Announcement()
            {
                AnnouncementId = 2,
                CreatedAt = DateTime.Now.AddDays(-10),
                Price = 100,
                RealtyInformationId = 2,
                RentUserId = 2,
                City = "Київ",
                Title = "Оренда квартири дешево писати лс",
                Description = "Тут тіпа тексту речень 6 буде",
                RealtyInformation = new RealtyInformation()
                {
                    RealtyInformationId = 2,
                    HeatingType = "Власне",
                    Square = 90,
                    Year = 2019,
                    RoomCount = 3,
                    Address = "Келецька 39/32",
                    Images = new List<ImageInfo>()
                    {
                        new ImageInfo()
                        {
                            ImageInfoId = 3,
                            Name = "Test2.png",
                            Path = "http://res.cloudinary.com/didqycjsn/image/upload/v1617222992/uypilxn7u2dcb469qyi2.png",
                            RealtyInformationId = 2
                        }
                    }
                }

            },
            new Announcement()
            {
                AnnouncementId = 3,
                CreatedAt = DateTime.Now.AddDays(-5),
                Price = 2321,
                RealtyInformationId = 3,
                RentUserId = 3,
                City = "Вінниця",
                Title = "Оренда квартири дешево писати лс",
                Description = "Тут тіпа тексту речень 6 буде",
                RealtyInformation = new RealtyInformation()
                {
                    RealtyInformationId = 3,
                    HeatingType = "Індивідуальне",
                    Square = 40.2,
                    Year = 2000,
                    RoomCount = 1,
                    Address = "Шевченка 1148",
                    Images = new List<ImageInfo>()
                    {
                        new ImageInfo()
                        {
                            ImageInfoId = 4,
                            Name = "Test1.png",
                            Path = "http://res.cloudinary.com/didqycjsn/image/upload/v1617222992/uypilxn7u2dcb469qyi2.png",
                            RealtyInformationId = 3
                        },
                        new ImageInfo()
                        {
                            ImageInfoId = 5,
                            Name = "Test2.png",
                            Path = "http://res.cloudinary.com/didqycjsn/image/upload/v1617223053/mm0ws5jkrlixraialg1g.png",
                            RealtyInformationId = 3
                        },
                        new ImageInfo()
                        {
                            ImageInfoId = 6,
                            Name = "Test2.png",
                            Path = "http://res.cloudinary.com/didqycjsn/image/upload/v1617223053/mm0ws5jkrlixraialg1g.png",
                            RealtyInformationId = 3
                        }
                    }
                }

            }
        };

        public void Add(Announcement an)
        {
            anCounter++;
            if (an.RealtyInformation.Images.Count() >= 1)
            {
                foreach (var image in an.RealtyInformation.Images)
                {
                    imageCounter++;
                    image.RealtyInformationId = anCounter;
                    image.ImageInfoId = imageCounter;
                }
            }

            an.AnnouncementId = anCounter;
            an.RealtyInformationId = anCounter;
            an.RealtyInformation.RealtyInformationId = anCounter;
            storage.Add(an);
            
        }

        public void Add(RentUser user)
        {
            throw new NotImplementedException();
        }

        public Announcement GetById(int id)
        {
            return storage.FirstOrDefault(x => x.AnnouncementId == id);
        }

        public List<Announcement> GetFilter(SearchFilter filter)
        {
            var res = storage
                .Where(x => x.Price >= filter.FromPrice && x.Price <= filter.ToPrice &&
                            //x.City.Contains(filter.City) &&
                            x.RealtyInformation.RoomCount >= filter.FromRoomCount &&
                            x.RealtyInformation.RoomCount <= filter.ToRoomCount &&
                            //string.Equals(x.RealtyInformation.HeatingType, filter.HeatingType) &&
                            x.RealtyInformation.Year >= filter.FromYear &&
                            x.RealtyInformation.Year <= filter.ToYear &&
                            x.RealtyInformation.Square >= filter.FromSquare &&
                            x.RealtyInformation.Square <= filter.ToSquare);
                    

          /*  var rents = res.SelectMany(x => x.Rents.Where(y =>
                (y.FromDate < filter.FromDate && y.ToDate < filter.FromDate) ||
                (y.FromDate > filter.ToDate && y.ToDate > filter.ToDate)));

            var goodRents = rents.Select(x => x.AnnouncementId);

            res = res.Where(x => goodRents.Contains(x.AnnouncementId));*/

                var result = res
                    .OrderBy(x => x.CreatedAt)
                    .Skip((filter.Page - 1) * filter.Items)
                    .Take(filter.Items).ToList();


            return result;
        }

        public int GetFilterCount(SearchFilter filter)
        {
            var res = storage
                .Where(x => x.Price >= filter.FromPrice && x.Price <= filter.ToPrice &&
                           // x.City.Contains(filter.City) &&
                            x.RealtyInformation.RoomCount >= filter.FromRoomCount &&
                            x.RealtyInformation.RoomCount <= filter.ToRoomCount &&
                            //string.Equals(x.RealtyInformation.HeatingType, filter.HeatingType) &&
                            x.RealtyInformation.Year >= filter.FromYear &&
                            x.RealtyInformation.Year <= filter.ToYear &&
                            x.RealtyInformation.Square >= filter.FromSquare &&
                            x.RealtyInformation.Square <= filter.ToSquare);


            /*  var rents = res.SelectMany(x => x.Rents.Where(y =>
                  (y.FromDate < filter.FromDate && y.ToDate < filter.FromDate) ||
                  (y.FromDate > filter.ToDate && y.ToDate > filter.ToDate)));

              var goodRents = rents.Select(x => x.AnnouncementId);

              res = res.Where(x => goodRents.Contains(x.AnnouncementId));*/




            return res.Count();
        }

        public List<Announcement> GetRent()
        {
            return storage;
        }

        public string Login()
        {
            var tokenTimeCreated = DateTime.UtcNow;

            var claims = new List<Claim>
            {
                new Claim("UserName", "TestUserName"),
            };

            string secretKey = "1144apiSynca11api41";
            int expires = int.Parse("600");

            var jwt = new JwtSecurityToken(
                issuer: "Me",
                audience: "My Api",
                notBefore: tokenTimeCreated,
                claims: claims,
                expires: tokenTimeCreated.AddMinutes(expires),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(secretKey)),
                    SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        public void Rent(int announcementId)
        {
            storage.FirstOrDefault(x => x.AnnouncementId == announcementId).Rents
                .Add(new RentInformation()
                {
                    RentInformationId = rentCounter++
                });
        }
    }
}
