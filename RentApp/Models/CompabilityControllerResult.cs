﻿namespace RentApp.Models
{
    public class CompabilityControllerResult
    {
        public int Compability { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }

        public string Age { get; set; }

        public string Review { get; set; }

        public string Sex { get; set; }
    }
}
