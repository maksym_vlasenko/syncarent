﻿namespace RentApp.Models
{
    public class CompatibleUserProfile
    {
        public string Age { get; set; }
        public string Sex { get; set; }
        public string Temperament { get; set; }

        public string Cook { get; set; }

        public string Activity { get; set; }

        public string Rest { get; set; }

        public string Status { get; set; }

        public string Conflict { get; set; }

        public string Pet { get; set; }

        public string Review { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }
    }
}