﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using RentApp.Data.Entities;
using RentApp.Models;

namespace RentApp
{
    public interface IStorage
    {
        void Add(Announcement an);
        Announcement GetById(int id);

        List<Announcement> GetFilter(SearchFilter filter);

        List<Announcement> GetRent();

        int GetFilterCount(SearchFilter filter);

        void Rent(int announcementId);

        void Add(RentUser user);

        string Login();
    }
}
